(* TODO: possibly more restrictive in tokarrset and tokarrcpy with asserts instead of premature returns *)
MODULE jsmntest;

IMPORT jsmn, Strings, Out;

PROCEDURE check(cond : BOOLEAN; tname : ARRAY OF CHAR; tno : INTEGER);
BEGIN
    Out.String(tname);
    Out.String(" , test no. ");
    Out.Int(tno, 0);
    IF(~cond) THEN
        Out.String(" FAILED!");
    ELSE
        Out.String(" passed!");
    END;
    Out.Ln;
END check;

PROCEDURE TOKENEQ(t : jsmn.TJSMNTok; tokstart, tokend : INTEGER; toktype : INTEGER) : BOOLEAN;
BEGIN
    RETURN (t.start = tokstart) & (t.end = tokend) & (t.type = toktype);
END TOKENEQ;

PROCEDURE TOKENSTRING(js : ARRAY OF CHAR; t : jsmn.TJSMNTok; s : ARRAY OF CHAR) : BOOLEAN;
VAR
    ls : ARRAY 16 OF CHAR;
    same : BOOLEAN;
BEGIN
    Strings.Extract(js, t.start, (t.end - t.start), ls);
    same := (Strings.Compare(ls, s) = 0);
    RETURN (Strings.Length(s) = (t.end - t.start)) & same;
END TOKENSTRING;

PROCEDURE TOKENPRINT(t : jsmn.TJSMNTok);
BEGIN
    Out.String("start: ");
    Out.Int(t.start, 0);
    Out.String(", end: ");
    Out.Int(t.end, 0);
    Out.String(", type: ");
    CASE t.type OF
        jsmn.TypPrimitive : Out.String("TypPrimitive");
        | jsmn.TypObject : Out.String("TypObject");
        | jsmn.TypArray : Out.String("TypArray");
        | jsmn.TypString : Out.String("TypString");
    ELSE
        Out.String("UNKNOWN"); (* in regards to ports to lang without enum type *)
    END;
    Out.String(", size: ");
    Out.Int(t.size, 0);
    Out.Ln;
END TOKENPRINT;

PROCEDURE ERRORPRINT(e : INTEGER);
BEGIN
    Out.String("Error: ");
    CASE e OF
        jsmn.ErrorNo : Out.String("ErrorNo");
        | jsmn.ErrorNoMem : Out.String("ErrorNoMem");
        | jsmn.ErrorInval : Out.String("ErrorInval");
        | jsmn.ErrorPart : Out.String("ErrorPart");
    ELSE
        Out.String("UNKNOWN"); (* in regards to ports to lang without enum type *)
    END;
    Out.Ln;
END ERRORPRINT;

PROCEDURE tokarrset(VAR ta : ARRAY OF jsmn.TJSMNTok; v : INTEGER);
VAR
    sz, i : INTEGER;
BEGIN
    sz := LEN(ta);
    IF (sz = 0) THEN RETURN; END;
    (*
    Out.String("memset sz: ");
    Out.Int(sz, 0);
    Out.String(" -- memset szel: ");
    Out.Int(szel, 0);
    Out.Ln;
    *)
    FOR i := 0 TO sz-1 DO
        ta[i].type := jsmn.TypPrimitive;
        ta[i].start := v;
        ta[i].end := v;
        ta[i].size := v;
    END;
END tokarrset;

PROCEDURE tokarrcpy(VAR ta, tb : ARRAY OF jsmn.TJSMNTok; n : INTEGER);
VAR
    sza, szb, i : INTEGER;
BEGIN
    sza := LEN(ta);
    szb := LEN(tb);
    IF (sza = 0) OR (szb = 0) THEN RETURN; END;
    IF (sza < n) OR (szb < n) THEN RETURN; END;

    FOR i:=0 TO  n-1 DO
        ta[i] := tb[i];
    END;
END tokarrcpy;

PROCEDURE testempty();
VAR
    js : ARRAY 16 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 16 OF CHAR;
    i : INTEGER;
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testempty", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append("{}", js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check(r >= 0, n, i);
    i := 2;
    check(t[0].type = jsmn.TypObject, n, i);
    i := 3;
    check((t[0].start = 0) & (t[0].end = 2), n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append("[]", js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 4;
    check(r >= 0, n, i);
    i := 5;
    check(t[0].type = jsmn.TypArray, n, i);
    i := 6;
    check((t[0].start = 0) & (t[0].end = 2), n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"a":[]}', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 7;
    check(r >= 0, n ,i);
    i := 8;
    check((t[0].type = jsmn.TypObject) & (t[0].start = 0) & (t[0].end = 8), n, i);
    i := 9;
    check((t[1].type = jsmn.TypString) & (t[1].start = 2) & (t[1].end = 3), n, i);
    i := 10;
    check((t[2].type = jsmn.TypArray) & (t[2].start = 5) & (t[2].end = 7), n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append("[{},{}]", js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 11;
    check(r >= 0, n ,i);
    i := 12;
    check((t[0].type = jsmn.TypArray) & (t[0].start = 0) & (t[0].end = 7), n ,i);
    i := 13;
    check((t[1].type = jsmn.TypObject) & (t[1].start = 1) & (t[1].end = 3), n ,i);
    i := 14;
    check((t[2].type = jsmn.TypObject) & (t[2].start = 4) & (t[2].end = 6), n ,i);
END testempty;

PROCEDURE testsimple();
VAR
    js : ARRAY 16 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 16 OF CHAR;
    i : INTEGER;
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testsimple", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"a": 0}', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check(r >= 0, n ,i);
    i := 2;
    check(TOKENEQ(t[0], 0, 8, jsmn.TypObject), n ,i);
    i := 3;
    check(TOKENEQ(t[1], 2, 3, jsmn.TypString), n ,i);
    i := 4;
    check(TOKENEQ(t[2], 6, 7, jsmn.TypPrimitive), n ,i);
    i := 5;
    check(TOKENSTRING(js, t[0], js), n ,i);
    i := 6;
    check(TOKENSTRING(js, t[1], "a"), n ,i);
    i := 7;
    check(TOKENSTRING(js, t[2], "0"), n ,i);
END testsimple;

PROCEDURE testprimitive();
VAR
    js : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 16 OF CHAR;
    i : INTEGER;
BEGIN
	IF (~jsmn.Strict()) THEN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testprimitive", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"boolVar" : true', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check((r >= 0) & (t[0].type = jsmn.TypString) & (t[1].type = jsmn.TypPrimitive), n ,i);
    i := 2;
    check(TOKENSTRING(js, t[0], "boolVar"), n ,i);
    i := 3;
    check(TOKENSTRING(js, t[1], "true"), n ,i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"boolVar" : false', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 4;
    check((r >= 0) & (t[0].type = jsmn.TypString) & (t[1].type = jsmn.TypPrimitive), n ,i);
    i := 5;
    check(TOKENSTRING(js, t[0], "boolVar"), n ,i);
    i := 6;
    check(TOKENSTRING(js, t[1], "false"), n ,i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"intVar" : 12345', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 7;
    check((r >= 0) & (t[0].type = jsmn.TypString) & (t[1].type = jsmn.TypPrimitive), n ,i);
    i := 8;
    check(TOKENSTRING(js, t[0], "intVar"), n ,i);
    i := 9;
    check(TOKENSTRING(js, t[1], "12345"), n ,i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"floatVar" : 12.345', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 10;
    check((r >= 0) & (t[0].type = jsmn.TypString) & (t[1].type = jsmn.TypPrimitive), n ,i);
    i := 11;
    check(TOKENSTRING(js, t[0], "floatVar"), n ,i);
    i := 12;
    check(TOKENSTRING(js, t[1], "12.345"), n ,i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"nullVar" : null', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 13;
    check((r >= 0) & (t[0].type = jsmn.TypString) & (t[1].type = jsmn.TypPrimitive), n ,i);
    i := 14;
    check(TOKENSTRING(js, t[0], "nullVar"), n ,i);
    i := 15;
    check(TOKENSTRING(js, t[1], "null"), n ,i);
  ELSE
  	Out.String("non-Strict, no testprimitive"); Out.Ln();
  END;
END testprimitive;

PROCEDURE teststring();
VAR
    js, ks : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 16 OF CHAR;
    i : INTEGER;
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("teststring", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"strVar" : "hello world"', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check((r >= 0) & (t[0].type = jsmn.TypString) & (t[1].type = jsmn.TypString), n, i);
    i := 2;
    check(TOKENSTRING(js, t[0], "strVar"), n, i);
    i := 3;
    check(TOKENSTRING(js, t[1], "hello world"), n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    (* Strings.Append('"strVar" : "escapes: ' + '\' + '/' + CHR(13) + CHR(10) + CHR(9) + CHR(8)+ CHR(12)+'"', js); *)
    (* js = "\"strVar\" : \"escapes: \\/\\r\\n\\t\\b\\f\\\"\\\\\""; *)
    Strings.Append('"strVar" : "escapes: ', js);
    Strings.Append('\', js);
    Strings.Append('/', js);
    Strings.Append(CHR(13), js);
    Strings.Append(CHR(10), js);
    Strings.Append(CHR(9), js);
    Strings.Append(CHR(8), js);
    (* Oberon has problems with form feed in sting?
    Strings.Append(CHR(12), js);
    *)
    Strings.Append('"', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 4;
    check((r >= 0) & (t[0].type = jsmn.TypString) & (t[1].type = jsmn.TypString), n, i);
    i := 5;
    check(TOKENSTRING(js, t[0], "strVar"), n, i);
    i := 6;
    Strings.Delete(ks, 0, Strings.Length(ks));
    Strings.Append('escapes: ', ks);
    Strings.Append('\', ks);
    Strings.Append('/', ks);
    Strings.Append(CHR(13), ks);
    Strings.Append(CHR(10), ks);
    Strings.Append(CHR(9), ks);
    Strings.Append(CHR(8), ks);
    (* Oberon has problems with form feed in sting?
    Strings.Append(CHR(12), ks);
    *)
    check(TOKENSTRING(js, t[1], ks), n, i);
END teststring;

PROCEDURE testpartialstring();
VAR
    js : ARRAY 32 OF CHAR;
    jsSlash : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 32 OF CHAR;
    i : INTEGER;
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testpartialstring", n);

    jsmn.Init(p);
    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"x" : "va', js);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check((r = jsmn.ErrorPart) & (t[0].type = jsmn.TypString), n, i);
    i := 2;
    check(TOKENSTRING(js, t[0], "x"), n, i);
    i := 3;
    check(p.toknext = 1, n, i);

		jsmn.Init(p);
		Strings.Delete(jsSlash, 0, Strings.Length(jsSlash));
		Strings.Append('"x": "va\"', jsSlash);
		r := jsmn.Parse(p, jsSlash, Strings.Length(jsSlash), t, 10);
		i := 4;
		check(r = jsmn.ErrorPart, n, i);
		
(* TODO
	jsmn_init(&p);
	char js_unicode[10] = "\"x\": \"va\\u";
	r = jsmn_parse(&p, js_unicode, sizeof(js_unicode), tok, 10);
	check(r == JSMN_ERROR_PART);
*)

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"x" : "valu', js);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 5;
    check((r = jsmn.ErrorPart) & (t[0].type = jsmn.TypString), n, i);
    i := 6;
    check(TOKENSTRING(js, t[0], "x"), n, i);
    i := 7;
    check(p.toknext = 1, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"x" : "value"', js);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 8;
    check((r >=0) & (t[0].type = jsmn.TypString), n, i);
    i := 9;
    check(TOKENSTRING(js, t[0], "x"), n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"x" : "value", "y" : "value y"', js);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 10;
    check((r >=0) & (t[0].type = jsmn.TypString) &
                    (t[1].type = jsmn.TypString) &
                    (t[2].type = jsmn.TypString) &
                    (t[3].type = jsmn.TypString), n, i);
    i := 11;
    check(TOKENSTRING(js, t[0], "x"), n, i);
    i := 12;
    check(TOKENSTRING(js, t[1], "value"), n, i);
    i := 13;
    check(TOKENSTRING(js, t[2], "y"), n, i);
    i := 14;
    check(TOKENSTRING(js, t[3], "value y"), n, i);
END testpartialstring;

PROCEDURE testunquotedkeys();
VAR
    js : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 32 OF CHAR;
    i : INTEGER;
BEGIN
	IF (~jsmn.Strict()) THEN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testunquotedkeys", n);

    jsmn.Init(p);
    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('key1: "value"', js);
    Strings.Append(CHR(10), js);
    Strings.Append('key2 : 123', js);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check((r >= 0) & (t[0].type = jsmn.TypPrimitive) &
                     (t[1].type = jsmn.TypString) &
                     (t[2].type = jsmn.TypPrimitive) &
                     (t[3].type = jsmn.TypPrimitive), n, i);
    i := 2;
    check(TOKENSTRING(js, t[0], "key1"), n, i);
    i := 3;
    check(TOKENSTRING(js, t[1], "value"), n, i);
    i := 4;
    check(TOKENSTRING(js, t[2], "key2"), n, i);
    i := 5;
    check(TOKENSTRING(js, t[3], "123"), n, i);
  ELSE
  	Out.String("non-Strict, no testunquotedkeys"); Out.Ln();
  END;
END testunquotedkeys;

PROCEDURE testpartialarray();
VAR
    js : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 32 OF CHAR;
    i : INTEGER;
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testpartialarray", n);

    jsmn.Init(p);
    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('[ 1, true, ', js);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check((r = jsmn.ErrorPart) & (t[0].type = jsmn.TypArray) &
                                     (t[1].type = jsmn.TypPrimitive) &
                                     (t[2].type = jsmn.TypPrimitive), n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('  [ 1, true, [123, "hello"]', js);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 2;
    check((r = jsmn.ErrorPart) &
          (t[0].type = jsmn.TypArray) &
          (t[1].type = jsmn.TypPrimitive) &
          (t[2].type = jsmn.TypPrimitive) &
          (t[3].type = jsmn.TypArray) &
          (t[4].type = jsmn.TypPrimitive) &
          (t[5].type = jsmn.TypString), n, i);
    (* check child nodes of the 2nd array *)
    i := 3;
    check(t[3].size = 2, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('  [ 1, true, [123, "hello"]]', js);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 4;
    check((r >= 0) & (t[0].type = jsmn.TypArray) &
          (t[1].type = jsmn.TypPrimitive) & (t[2].type = jsmn.TypPrimitive) &
          (t[3].type = jsmn.TypArray) & (t[4].type = jsmn.TypPrimitive) &
          (t[5].type = jsmn.TypString), n , i);
    i := 5;
    check(t[3].size = 2, n, i);
    i := 6;
    check(t[0].size = 3, n, i);
END testpartialarray;

PROCEDURE testarraynomem();
VAR
    js : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    tsmall, tlarge : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 32 OF CHAR;
    i, j : INTEGER;
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testarraynomem", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('  [ 1, true, [123, "hello"]]', js);

    FOR j := 0 TO 5 DO
        jsmn.Init(p);
        tokarrset(tsmall, 0);
        tokarrset(tlarge, 0);
        r := jsmn.Parse(p, js, Strings.Length(js), tsmall, j);
        i := 1 + j + 0;
        check(r = jsmn.ErrorNoMem, n, i);

        tokarrcpy(tlarge, tsmall, 10);

        r := jsmn.Parse(p, js, Strings.Length(js), tlarge, 10);
        i := 1 + j + 1;
        check(r >= 0, n, i);
        i := 1 + j + 2;
        check((tlarge[0].type = jsmn.TypArray) & (tlarge[0].size = 3), n, i);
        i := 1 + j + 3;
        check((tlarge[3].type = jsmn.TypArray) & (tlarge[3].size = 2), n, i);
    END;
END testarraynomem;

PROCEDURE testobjectsarrays();
VAR
    js : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 32 OF CHAR;
    i : INTEGER;
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testobjectsarrays", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('[10}', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check(r = jsmn.ErrorInval, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('[10]', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 2;
    check(r >= 0, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"a": 1]', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 3;
    check(r = jsmn.ErrorInval, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"a": 1}', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 4;
    check(r >= 0, n, i);

END testobjectsarrays;

PROCEDURE testissue22();
VAR
    js : ARRAY 1024 OF CHAR;
    (* stst : ARRAY 32 OF CHAR; *)
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 128 OF jsmn.TJSMNTok;
    n : ARRAY 32 OF CHAR;
    i : INTEGER;
    (* j : INTEGER; *)
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testissue22", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"height":10, "layers":[ { "data":[6,6], "height":10, "name":"Calque de Tile 1", "opacity":1, "type":"tilelayer", "visible":true, "width":10, "x":0, "y":0 }], "orientation":"orthogonal", "properties": { }, "tileheight":32, "tilesets":[ { "firstgid":1, "image":"../images/tiles.png", "imageheight":64, "imagewidth":160, "margin":0, "name":"Tiles", "properties":{}, "spacing":0, "tileheight":32, "tilewidth":32 }], "tilewidth":32, "version":1, "width":10 }', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 128);
    i := 1;
    check(r >= 0, n, i);
    (*
    j := 1;
    WHILE (t[j].end < t[0].end) DO
        IF ((t[j].type = jsmn.TypString) OR (t[j].type = jsmn.TypPrimitive)) THEN
            Strings.Delete(stst, 0, Strings.Length(stst));
            Strings.Extract(js, t[j].start, t[j].end - t[j].start, stst);
            Out.String(stst);
            Out.Ln;
        ELSIF (t[j].type = jsmn.TypArray) THEN
            Out.String("[ ");
            Out.Int(t[j].size, 0);
            Out.String(" elems ]");
            Out.Ln;
        ELSIF (t[j].type = jsmn.TypObject) THEN
            Out.String("{ ");
            Out.Int(t[j].size, 0);
            Out.String(" elems }");
            Out.Ln;
        ELSE
            TOKENPRINT(t[j]);
        END;
        INC(j);
    END;
    *)
END testissue22;

(* --- Currently no unicode support under M-2/O-07
PROCEDURE testunicodecharacters();
VAR
    js : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 32 OF CHAR;
    i : INTEGER;
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testunicodecharacters", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('"a":' + CHR() + , js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check(r = jsmn.ErrorInval, n, i);

END testunicodecharacters;
*)

PROCEDURE testinputlength();
VAR
    js : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 32 OF CHAR;
    i : INTEGER;
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testinputlength", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"a": 0}garbage', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, 8, t, 10);
    i := 1;
    check(r = 3, n, i);
    i := 2;
    check(TOKENSTRING(js, t[0], '{"a": 0}'), n, i);
    i := 3;
    check(TOKENSTRING(js, t[1], "a"), n, i);
    i := 4;
    check(TOKENSTRING(js, t[2], "0"), n, i);
END testinputlength;

PROCEDURE testcount();
VAR
    js : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 32 OF CHAR;
    i : INTEGER;
BEGIN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testcount", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{}', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check(r = 1, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('[]', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 2;
    check(r = 1, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('[[]]', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 3;
    check(r = 2, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('[[], []]', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 4;
    check(r = 3, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('[[], [[]], [[], []]]', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 5;
    check(r = 7, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('["a", [[], []]]', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 6;
    check(r = 5, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('[[], "[], [[]]", [[]]]', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 7;
    check(r = 5, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('[1, 2, 3]', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 8;
    check(r = 4, n, i);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('[1, 2, [3, "a"], null]', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 9;
    check(r = 7, n, i);
END testcount;

(* ATTN: strict only *)
PROCEDURE testkeyvalue();
VAR
    js : ARRAY 32 OF CHAR;
    r : INTEGER;
    p : jsmn.TJSMNParser;
    t : ARRAY 10 OF jsmn.TJSMNTok;
    n : ARRAY 32 OF CHAR;
    i : INTEGER;	
BEGIN
	IF (jsmn.Strict()) THEN
    Strings.Delete(n, 0, Strings.Length(n));
    Strings.Append("testkeyvalue", n);

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"a": 0, "b": "c"}', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
    i := 1;
    check(t[0].size = 2, n, i);
    i := 2;
    check((t[1].size = 1) & (t[3].size = 1), n, i);
    i := 3;
    check((t[2].size = 0) & (t[4].size = 0), n, i);
		
		(* TODO
		js = "{\"a\"\n0}";
		jsmn_init(&p);
		r = jsmn_parse(&p, js, strlen(js), tokens, 10);
		check(r == JSMN_ERROR_INVAL);
		*)

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"a", 0}', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
		i := 4;
		Out.String("r = "); Out.Int(r, 0); Out.Ln();
		check(r = jsmn.ErrorInval, n, i);	
			
    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"a": {2}}', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
		i := 5;
		Out.String("r = "); Out.Int(r, 0); Out.Ln();
		check(r = jsmn.ErrorInval, n, i);	
			
    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"a": {2: 3}}', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
		i := 6;
		Out.String("r = "); Out.Int(r, 0); Out.Ln();
		check(r = jsmn.ErrorInval, n, i);	

    Strings.Delete(js, 0, Strings.Length(js));
    Strings.Append('{"a": {"a": 2 3}}', js);
    jsmn.Init(p);
    r := jsmn.Parse(p, js, Strings.Length(js), t, 10);
		i := 7;
		Out.String("r = "); Out.Int(r, 0); Out.Ln();
		check(r = jsmn.ErrorInval, n, i);	
	ELSE
		Out.String("non-Strict, no testkeyvalue"); Out.Ln();
	END;
END testkeyvalue;

BEGIN
    testempty();
    testsimple();
    testprimitive();
    teststring();
    testpartialstring();
    testunquotedkeys();
    testpartialarray();
    testarraynomem();
    testobjectsarrays();
    testissue22();
    (* testunicodecharacters(); *)
    testinputlength();
    testcount();
    testkeyvalue();
END jsmntest.
