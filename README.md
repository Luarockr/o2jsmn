o2jsmn - simple Jason parser/tokeizer port in Oberon-2 of jsmn: https://github.com/zserge/jsmn

compile:
obc jsmn.m jsmntest.m
with Oxford Oberon-2 Compiler: https://bitbucket.org/Spivey/obc-3/overview