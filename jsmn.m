MODULE jsmn;

(* IMPORT Out; *)

CONST 
  TypPrimitive* = 0;
  TypObject* = 1;
  TypArray* = 2;
  TypString* = 3;

  (* no error *)
  ErrorNo* = 0;
  (* Not enough tokens were provided *)
  ErrorNoMem* = -1;
  (* Invalid character inside JSON string *)
  ErrorInval* = -2;
  (* The string is not a full JSON packet, more bytes expected *)
  ErrorPart* = -3;

TYPE
  TJSMNTok* = RECORD
    type* : INTEGER;
    start* : INTEGER;
    end* : INTEGER;
    size* : INTEGER;
    parent* : INTEGER;
  END;


  TJSMNParser* = RECORD
    pos* : INTEGER;
    toknext* : INTEGER;
    toksuper* : INTEGER
  END;

VAR
	parentLinks : BOOLEAN;
	strictMode : BOOLEAN;

(* returns the index to the allocated token inside tokens array *)
PROCEDURE AllocToken(VAR parser : TJSMNParser;
                           VAR tokens : ARRAY OF TJSMNTok;
                           numTokens : INTEGER) : INTEGER;
VAR
    r : INTEGER;
BEGIN
    IF (parser.toknext >= numTokens) THEN r := -1;
    ELSE
        r := parser.toknext;
        (* token := tokens[parser.toknext]; *)
        INC(parser.toknext);
        tokens[r].start := -1;
        tokens[r].end := -1;
        tokens[r].size := 0;
        IF (parentLinks) THEN tokens[r].parent := -1; END;
    END;
    RETURN r;
END AllocToken;

PROCEDURE FillToken(ti : INTEGER; VAR tokens : ARRAY OF TJSMNTok; type : INTEGER; start : INTEGER; end : INTEGER);
BEGIN
    tokens[ti].type := type;
    tokens[ti].start := start;
    tokens[ti].end := end;
    tokens[ti].size := 0;
END FillToken;

PROCEDURE ParsePrimitive(VAR parser : TJSMNParser;
                               js : ARRAY OF CHAR;
                               len : INTEGER;
                               VAR tokens : ARRAY OF TJSMNTok;
                               numTokens : INTEGER) : INTEGER;
VAR
    start : INTEGER;
    found : BOOLEAN;
    it : INTEGER;
    (*token : TJSMNTok;*)
BEGIN
    start := parser.pos;
    found := FALSE;
    it := 0;

    WHILE ((parser.pos < len) & (ORD(js[parser.pos]) # 0) & (~found)) DO
    	IF (~strictMode) THEN
			CASE ORD(js[parser.pos]) OF
				ORD(':'), 9 (* \t *), 13 (* \r *), 10 (* \n ... was 12? *),ORD(' '),ORD(','),ORD(']'),ORD('}') : found := TRUE;
			ELSE; (* intentially left empty *)
			END;
		ELSE (* strict *)
			CASE ORD(js[parser.pos]) OF
				9 (* \t *), 13 (* \r *), 10 (* \n ... was 12? *),ORD(' '),ORD(','),ORD(']'),ORD('}') : found := TRUE;
			ELSE; (* intentially left empty *)	
			END;		
		END; (* strict *)
        IF (~found) & (ORD(js[parser.pos]) < 32) OR (ORD(js[parser.pos]) > 127) THEN
            parser.pos := start;
            RETURN ErrorInval;
        END;
        IF (~found) THEN INC(parser.pos); END;
    END; (* while *)
    
    IF (strictMode) & (~found) THEN
    		(* In strict mode primitive must be followed by a comma/object/array *)
				parser.pos := start;
				RETURN ErrorPart;
    END; (* strict *)

    (* IF (found) THEN *)
        (*
        if (tokens == NULL) {
            parser->pos--;
            return 0;
        }
        *)
        it := AllocToken(parser, tokens, numTokens);
        IF (it = -1) THEN
            parser.pos := start;
            RETURN ErrorNoMem;
        END;
        FillToken(it, tokens, TypPrimitive, start, parser.pos);
				IF (parentLinks) THEN tokens[it].parent := parser.toksuper; END;
        DEC(parser.pos);
        RETURN ErrorNo;
    (* END; *)
    RETURN ErrorInval;
END ParsePrimitive;

PROCEDURE ParseString(VAR parser : TJSMNParser;
                            js : ARRAY OF CHAR;
                            len : INTEGER;
                            VAR tokens : ARRAY OF TJSMNTok;
                            numTokens : INTEGER) : INTEGER;
VAR
    start : INTEGER;
    (*token : TJSMNTok;*)
    c : CHAR;
    i, it : INTEGER;
BEGIN
    start := parser.pos;
    INC(parser.pos);

    (* Skip starting quote *)
    WHILE ((parser.pos < len) & (ORD(js[parser.pos]) # 0)) DO
        c := js[parser.pos];
        (* Quote: end of string *)
        IF (ORD(c) = ORD('"')) THEN
            (*
            if (tokens == NULL) {
                parser->pos--;
                return 0;
            }
            *)
            it := AllocToken(parser, tokens, numTokens);
            IF (it = -1) THEN
                parser.pos := start;
                RETURN ErrorNoMem;
            END;
            FillToken(it, tokens, TypString, start+1, parser.pos);
						IF (parentLinks) THEN
							tokens[it].parent := parser.toksuper;
						END;
            RETURN ErrorNo;
        END;
        
        (* Backslash: Quoted symbol expected *)
        IF (ORD(c) = ORD('\')) & (parser.pos + 1 < len) THEN
            INC(parser.pos);
            CASE ORD(js[parser.pos]) OF
                ORD('"'), ORD('/'), ORD('\'), ORD('b'), ORD('f'), ORD('r'), ORD('n'), ORD('t'): ;
                | ORD('u') :
                    INC(parser.pos);
                    i := 0;
                    WHILE ((i < 4) & (parser.pos < len) & (ORD(js[parser.pos]) # 0)) DO
                        CASE ORD(js[parser.pos]) OF
                            48..57, 65..79, 97..102 : parser.pos := start;
                                RETURN ErrorInval;
                            ELSE; (* intentionally left empty *)
                        END;
                        INC(parser.pos);
                        INC(i);
                    END; (* inner while *)
                    DEC(parser.pos);
            ELSE
                parser.pos := start;
                RETURN ErrorInval;
            END;
        END;
        INC(parser.pos);
    END; (* while*)
    parser.pos := start;
    RETURN ErrorPart;
END ParseString;


PROCEDURE Parse*(VAR parser : TJSMNParser;
                     js : ARRAY OF CHAR;
                     len : INTEGER;
                     VAR tokens : ARRAY OF TJSMNTok;
                     numTokens : INTEGER) : INTEGER;
VAR
    r : INTEGER;
    i : INTEGER;
    (* token : TJSMNTok; *)
    count : INTEGER;
    c : CHAR;
    type : INTEGER;
    break : BOOLEAN;
    it : INTEGER;
    pi : INTEGER;
BEGIN
    count := 0;
    WHILE ((parser.pos < len) & (ORD(js[parser.pos]) # 0)) DO
        c := js[parser.pos];
        CASE ORD(c) OF
            ORD('{'), ORD('[') :
                (* Out.String("count++ '{', '['"); Out.Ln(); *)
                INC(count);
                (*
                if (tokens == NULL) {
                    break;
                }
                *)
                it := AllocToken(parser, tokens, numTokens);
                IF (it = -1) THEN RETURN ErrorNoMem; END;
                IF (parser.toksuper # -1) THEN 
                	INC(tokens[parser.toksuper].size); 
									IF (parentLinks) THEN
										tokens[it].parent := parser.toksuper;
									END;
                END;
                IF (c = '{') THEN tokens[it].type := TypObject; ELSE tokens[it].type := TypArray; END;
                tokens[it].start := parser.pos;
                parser.toksuper := parser.toknext - 1;
            |ORD('}'),ORD(']') :
                (*
                if (tokens == NULL) {
                    break;
                }
                *)
                IF (c = '}') THEN type := TypObject; ELSE type := TypArray; END;
                IF (parentLinks) THEN
									IF (parser.toknext < 1) THEN RETURN ErrorInval; END;
									pi := parser.toknext - 1;
									(* token := tokens[parser.toknext - 1]; *)
									break := FALSE;
									WHILE (~break) DO
										IF (tokens[pi].start # -1) & (tokens[pi].end = -1) THEN
											IF (tokens[pi].type # type) THEN RETURN ErrorInval; END;
											tokens[pi].end := parser.pos + 1;
											parser.toksuper := tokens[pi].parent;
											break := TRUE;
										END;
										IF (tokens[pi].parent = -1) THEN break := TRUE; END;
										IF (~break) THEN 
											pi := tokens[pi].parent;
											(* token := tokens[token.parent]; *)
										END;
									END;
                ELSE
									i := parser.toknext - 1;
									break := FALSE;
									WHILE ((i >= 0) & ~break) DO
											(*token := tokens[i];*)
											IF (tokens[i].start # -1) & (tokens[i].end = -1) THEN
													IF (tokens[i].type # type) THEN RETURN ErrorInval; END;
													parser.toksuper := -1;
													tokens[i].end := parser.pos + 1;
													break := TRUE;
											END;
											IF (~break) THEN DEC(i); END;
									END; (* while *)
									(* Error if unmatched closing bracket *)
									IF (i = -1) THEN RETURN ErrorInval; END;
									break := FALSE;
									WHILE ((i >= 0) & ~break) DO
											(*token := tokens[i];*)
											IF (tokens[i].start # -1) & (tokens[i].end = -1) THEN
													parser.toksuper := i;
													break := TRUE;
											END;
											IF (~break) THEN DEC(i); END;
									END; (* while *)
                END; (* if parent link *)
            |ORD('"') :
                r := ParseString(parser, js, len, tokens, numTokens);
                IF (r < ErrorNo) THEN RETURN r; END;
                (* Out.String('count++ "'); Out.Ln(); *)
                INC(count);
                IF (parser.toksuper # -1) (* & tokens != NULL *) THEN
                    INC(tokens[parser.toksuper].size);
                END;
            |9,13,10,ORD(' ') : ;
            |ORD(':') :
            		parser.toksuper := parser.toknext - 1;
            |ORD(',') :
            	IF ((* tokens # NULL & *) 
            			(tokens[parser.toksuper].type # TypArray) &
									(tokens[parser.toksuper].type # TypObject)) THEN
									IF (parentLinks) THEN
										parser.toksuper := tokens[parser.toksuper].parent;
									ELSE
										break := FALSE;
										i := parser.toknext - 1;
										WHILE ((i >= 0) & ~break) DO
											IF (tokens[i].type = TypArray) OR (tokens[i].type = TypObject) THEN
												IF (tokens[i].start # -1) & (tokens[i].end = -1) THEN
													parser.toksuper := i;
													break := TRUE;
												END;
											END;
											DEC(i);
										END;
									END;
							END;
        ELSE (* case ord(c) *)
        	IF (strictMode) THEN
        		CASE ORD(c) OF
        			(* In strict mode primitives are: numbers and booleans *)
							ORD('-'), ORD('0'), ORD('1'), ORD('2'), ORD('3'), ORD('4'),
							ORD('5'), ORD('6'), ORD('7'), ORD('8'), ORD('9'),
							ORD('t'), ORD('f'), ORD('n') :
								(* And they must not be keys of the object *)
								pi := parser.toksuper;
								IF (tokens[pi].type = TypObject) OR 
									 ((tokens[pi].type = TypString) & (tokens[pi].size # 0)) THEN
									RETURN ErrorInval;
								END;
								r := ParsePrimitive(parser, js, len, tokens, numTokens);
								IF (r < ErrorNo) THEN RETURN r; END;
								(* Out.String("count++ default strict"); Out.Ln(); *)
								INC(count);
								IF (parser.toksuper # -1) (* & (tokens # NULL) *) THEN
									INC(tokens[parser.toksuper].size);
								END;
        		ELSE (* strict case ord(c) *)
	        		RETURN ErrorInval;
        		END; 
        	ELSE (* strict *)
            r := ParsePrimitive(parser, js, len, tokens, numTokens);
            IF (r < ErrorNo) THEN RETURN r; END;
            (* Out.String("count++ default"); Out.Ln(); *)
            INC(count);
            IF (parser.toksuper # -1) (* & (tokens # NULL) *) THEN
                INC(tokens[parser.toksuper].size);
            END;
          END; (* strict *)
        END; (* case *)
        INC(parser.pos);
    END; (* while *)

    i := parser.toknext - 1;
    WHILE (i>=0) DO
        IF (tokens[i].start # -1) & (tokens[i].end = -1) THEN
            RETURN ErrorPart;
        END;
        DEC(i);
    END;
    RETURN count;
END Parse;

PROCEDURE SetStrict*(x : BOOLEAN);
BEGIN
	strictMode := x;
END SetStrict;

PROCEDURE Strict*() : BOOLEAN;
BEGIN
	RETURN strictMode;
END Strict;

PROCEDURE SetParentLinks*(x : BOOLEAN);
BEGIN
	parentLinks := x;
END SetParentLinks;

PROCEDURE ParentLinks*() : BOOLEAN;
BEGIN
	RETURN parentLinks;
END ParentLinks;

PROCEDURE Init*(VAR parser : TJSMNParser);
BEGIN
		parentLinks := FALSE;
		strictMode := FALSE;
    parser.pos := 0;
    parser.toknext := 0;
    parser.toksuper := -1;
END Init;

END jsmn.
